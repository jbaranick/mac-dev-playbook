#!/usr/bin/env bash
set -o pipefail

function fatal() { echo -e "ERROR: $*" 1>&2; exit 1; }

function prepare() {
  sudo -p "Enter your system password: " echo "Starting bootstrap..."
  mkdir -p "$HOME/Source/system"
}

function get_macos_version() {
  local macos_vers
  macos_vers=$(sw_vers -productVersion) || fatal "Failed to determine macos version: $?"
  echo "${macos_vers%%.*}.$(x="${macos_vers#*.}"; echo "${x%%.*}")"
}

function version_gt() {
  [[ "${1%.*}" -gt "${2%.*}" ]] || [[ "${1%.*}" -eq "${2%.*}" && "${1#*.}" -gt "${2#*.}" ]]
}

function version_ge() {
  [[ "${1%.*}" -gt "${2%.*}" ]] || [[ "${1%.*}" -eq "${2%.*}" && "${1#*.}" -ge "${2#*.}" ]]
}

function version_lt() {
  [[ "${1%.*}" -lt "${2%.*}" ]] || [[ "${1%.*}" -eq "${2%.*}" && "${1#*.}" -lt "${2#*.}" ]]
}

function should_install_command_line_tools() {
  if version_ge "$macos_vers" "10.13" ]]; then
    ! [[ -e "/Library/Developer/CommandLineTools/usr/bin/git" ]]
  else
    ! [[ -e "/Library/Developer/CommandLineTools/usr/bin/git" ]] ||
      ! [[ -e "/usr/include/iconv.h" ]]
  fi
}

function installXcodeCommandLineTools() {
  if should_install_command_line_tools; then
    echo ""
    echo "Installing XCode commandline tools..."
    local in_progress=/tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
    touch ${in_progress}
    trap "[[ -f \"$in_progress\" ]] && rm \"$in_progress\"" EXIT
    if version_ge "$macos_vers" "10.15"; then
      cmd_line_tools=$(softwareupdate -l | awk '/\*\ Label: Command Line Tools/ { $1=$1;print }' | sed 's/^[[ \t]]*//;s/[[ \t]]*$//;s/*//' | cut -c 9-)	 || fatal "Failed to determine product: $?"
    elif version_gt "$macos_vers" "10.9"; then
      cmd_line_tools=$(softwareupdate -l | awk '/\*\ Command Line Tools/ { $1=$1;print }' | grep "$macos_vers" | sed 's/^[[ \t]]*//;s/[[ \t]]*$//;s/*//' | cut -c 2-) || fatal "Failed to determine product: $?"
    fi
    if (( $(grep -c . <<<"$cmd_line_tools") > 1 )); then
      cmd_line_tools_output="$cmd_line_tools"
      cmd_line_tools=$(printf "$cmd_line_tools_output" | tail -1)
    fi
    softwareupdate -i "$cmd_line_tools" --verbose || (rm ${in_progress} && fatal "Failed install XCode commandline tools: $?")
    sudo xcode-select --switch /Library/Developer/CommandLineTools || fatal "Failed switch to XCode commandline tools: $?"
  fi
}

function installHomebrew() {
  if ! command -v brew &>/dev/null; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" || fatal "Failed to install homebrew: $?"
  fi
}

function installOpenssl() {
  if ! brew list --formula | grep -E -e "^openssl@1.1$" &>/dev/null; then
    brew install openssl@1.1 || fatal "Failed to install openssl: $?"
  fi
}

function installPip() {
  local python_command=""
  if hash python3 2>/dev/null; then
    python_command="python3"
    pip_command="pip3"
  else
    python_command="python"
    pip_command="pip"
  fi
  if ! hash $pip_command 2>/dev/null; then
    echo ""
    echo "Installing pip..."
    curl -s https://bootstrap.pypa.io/get-pip.py | $python_command - --user || fatal "Failed to install pip: $?"
  fi
  export PYTHON_PIP_EXECUTABLE="$(which $pip_command)"
  $pip_command install virtualenv --user || fatal "Failed to install virtualenv: $?"
  local tmp_dir="$(mktemp -d)"
  $python_command -m virtualenv $tmp_dir -p $python_command
  source $tmp_dir/bin/activate
  export PYTHON_BIN_PATH="$($python_command -m site --user-base)/bin" || fatal "Failed to determine pip binary path: $?"
  export PATH=$PATH:$PYTHON_BIN_PATH
  export ANSIBLE_PYTHON_INTERPRETER="$(which $python_command)"
}

function installPackages() {
  if ! hash ansible 2>/dev/null; then
    echo ""
    echo "Installing ansible..."
    $pip_command install six || fatal "Failed to install six: $?"
    env LDFLAGS="-L$(brew --prefix openssl@1.1)/lib" CFLAGS="-I$(brew --prefix openssl@1.1)/include" $pip_command install cryptography || fatal "Failed to install cryptography: $?"
    $pip_command install ansible || fatal "Failed to install ansible: $?"
  fi
}

function installSdkHeaders() {
  if version_lt "$macos_vers" "10.15"; then
    echo ""
    echo "Installing SDK headers..."
    local os_version
    os_version="$(sw_vers -productVersion)" || fatal "Failed to determine os version: $?"
    sudo installer -pkg /Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_${os_version%.*}.pkg -target / || fatal "Failed to install SDK headers: $?"
  fi
}

function ensureConfigRepoExists() {
  local upstream="https://bitbucket.org/ensighten-ondemand/mac-dev-playbook-config-example"
  if [[ ! -d "$config_dir" ]]; then
    echo ""
    read -p "What is the name of your configuration repository? (mac-dev-playbook-config) " config_repository
    config_repository="${config_repository:-mac-dev-playbook-config}"
    if ! git ls-remote https://bitbucket.org/$bitbucket_username/$config_repository &>/dev/null; then
      echo ""
      echo "Fork 'mac-dev-playbook-config-example' as '$config_repository'"
      read -p "Press <enter> to open '$upstream'"
      open $upstream || fatal "Failed to open $upstream: $?"
      echo ""
      read -p "Press <enter> once you have forked the repository"
    fi
    echo ""
    echo "Cloning config repository $config_repository..."
    git clone "https://bitbucket.org/$bitbucket_username/$config_repository" "$config_dir" || fatal "Failed to clone $config_repository: $?"
    git -C $config_dir remote add upstream $upstream || fatal "Failed to add upstream remoate: $?"
    git -C $config_dir fetch origin || fatal "Failed to fetch origin: $?"
    git -C $config_dir fetch upstream || fatal "Failed to fetch upstream: $?"
  else
    git -C $config_dir remote update || fatal "Failed to update metadata: $?"
    local local_rev
    local_rev=$(git -C $config_dir rev-parse @) || fatal "Failed to get local revision: $?"
    local remote_rev
    remote_rev=$(git -C $config_dir rev-parse "@{u}") || fatal "Failed to get remote revision: $?"
    local base_rev
    base_rev=$(git -C $config_dir merge-base @ "@{u}") || fatal "Failed to get base revision: $?"
    if [[ "$local_rev" != "$remote_rev" && "$local_rev" == "$base_rev" ]]; then
      read -p "Would you like to update your configuration to match the BitBucket copy? (y,N) " -r
      echo ""
      if [[ $REPLY =~ ^[Yy]$ ]]; then
        git -C $config_dir pull || fatal "Failed to get pull changes: $?"
        echo ""
      fi
    fi
  fi
}

function initializeVaultConfig() {
  local vault_file="$config_dir/inventory/ensighten/group_vars/all/vault.yml"
  if [[ "$(head -n1 $vault_file)" != "\$ANSIBLE_VAULT;1.1;AES256" ]]; then
    echo ""
    read -p "Press <enter> to encrypt vault.yml"
    ansible-vault encrypt --ask-vault-pass $vault_file || fatal "Failed to encrypt $vault_file: $?"
    echo ""
    read -p "Press <enter> to edit vault.yml"
    ansible-vault edit --ask-vault-pass $vault_file || fatal "Failed to edit $vault_file: $?"
    if [[ `git -C $config_dir status --porcelain` ]]; then
      git -C $config_dir commit -am "Update secrets" || fatal "Failed to commit: $?"
      git -C $config_dir push || fatal "Failed to push changes: $?"
    fi
  fi
}

function modifyUserConfig() {
  local config_file="$config_dir/inventory/ensighten/group_vars/all/main.yml"
  read -p "Would you like to modify your main.yml? (y,N) " -r
  echo ""
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    "${EDITOR:-vi}" "$config_file"
    if [[ `git -C $config_dir  status --porcelain` ]]; then
      git -C $config_dir commit -am "Update configuration" || fatal "Failed to commit: $?"
      git -C $config_dir push || fatal "Failed to push changes: $?"
    fi
  fi
}

function modifyConfigRepo() {
  initializeVaultConfig
  modifyUserConfig
}

function runInstall() {
  echo ""
  read -p "Press <enter> to start install"
  cd $HOME || fatal "Failed to change directory to $HOME: $?"
  bash <(curl -s -H 'Cache-Control: no-cache' https://bitbucket.org/jbaranick/mac-dev-playbook/raw/master/install.sh) "$inventory_name"
}

function run() {
  local bitbucket_username="${1?"Bitbucket username was not specified"}"
  local inventory_name="${2:-ensighten}"
  local config_dir="$HOME/Source/system/mac-dev-playbook-config"
  local macos_vers
  macos_vers=$(get_macos_version)
  local pip_command=""
  if version_lt "$macos_vers" "10.10"; then
    fatal "MacOS 10.9 and earlier are not supporter"
  fi
  prepare
  installXcodeCommandLineTools
  installSdkHeaders
  installHomebrew
  installOpenssl
  installPip
  installPackages
  ensureConfigRepoExists
  modifyConfigRepo
  runInstall
  return 0
}

run "$@"

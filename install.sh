#!/usr/bin/env bash
set -o pipefail

function pushd() { command pushd "$@" > /dev/null; }
function fatal() { echo -e "ERROR: $*" 1>&2; exit 1; }

function syncMacDevPlaybook() {
  local config_dir="$HOME/Source/system/mac-dev-playbook"
  if [ ! -d $config_dir ]; then
    echo ""
    echo "Cloning mac-dev-playbook..."
    git clone https://jbaranick@bitbucket.org/jbaranick/mac-dev-playbook.git $config_dir || fatal "Failed to clone mac-dev-playbook: $?"
  else
    echo ""
    echo "Updating mac-dev-playbook..."
    git -C $config_dir pull &> /dev/null || fatal "Failed to git pull from mac-dev-playbook: $?"
  fi
  git -C $config_dir fetch origin || fatal "Failed to fetch origin: $?"
}

function installAnsibleGalaxyPackages() {
  echo ""
  echo "Installing ansible-galaxy packages..."
  ansible-galaxy install --force-with-deps -r requirements.yml || fatal "Failed to install ansible galaxy dependenies: $?"
}

function install() {
  echo ""
  echo "Running ansible playbook..."
  ansible-playbook main.yml -i $HOME/Source/system/mac-dev-playbook-config/inventory/${inventory_name} -K --ask-vault-pass --ask-become-pass -e "python_bin_path=$PYTHON_BIN_PATH" -e "python_pip_executable=$PYTHON_PIP_EXECUTABLE" || fatal "Failed to install via ansible: $?"
}

function run() {
  local inventory_name="${1?"Inventory name was not specified"}"
  sudo echo "Starting install..."
  mkdir -p $HOME/Source/system || fatal "Failed to create $HOME/Source/system: $?"
  syncMacDevPlaybook
  pushd $HOME/Source/system/mac-dev-playbook || fatal "Failed to change directory to $HOME/Source/system/mac-dev-playbook: $?"
  installAnsibleGalaxyPackages
  install
  return 0
}

run "$@"

# Mac Development Ansible Playbook

This playbook installs and configures most of the software I use on my Mac for web and software development. Some things in macOS are slightly difficult to automate, so I still have some manual installation steps, but at least it's all documented here.

This is a work in progress, and is mostly a means for me to document my current Mac's setup. I'll be evolving this set of playbooks over time.

*See also*:

  - [Boxen](https://github.com/boxen)
  - [Battleschool](http://spencer.gibb.us/blog/2014/02/03/introducing-battleschool)
  - [osxc](https://github.com/osxc)
  - [geerlingguy/mac-dev-playbook](https://github.com/geerlingguy/mac-dev-playbook) (the original inspiration for this project)
  - [MWGriffin/ansible-playbooks](https://github.com/MWGriffin/ansible-playbooks) (the original inspiration for geerlingguy/mac-dev-playbook)

## Installation

  1. Accept all Mac App Store apps from the account page in the Mac App Store
  2. Create a ssh public/private key by running: `ssh-keygen -p -m PEM -f ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa`
  3. Run this command from your $HOME directory: `bash <(curl -s -H 'Cache-Control: no-cache' https://bitbucket.org/jbaranick/mac-dev-playbook/raw/master/bootstrap.sh) <BITBUCKET_USERNAME>`
    1. Replace `<BITBUCKET_USERNAME>` with the name of your bitbucket user
  4. Perform manual steps

## Example Applications / Configuration

Applications (installed with Homebrew Cask):

  - 1password
  - atom
  - dropbox
  - font-firacode-nerd-font
  - font-microsoft-office
  - gitup
  - gitter
  - iterm2
  - java
  - joinme
  - keka
  - launchbar
  - launchcontrol
  - little-snitch
  - macdown
  - nvalt
  - postman
  - qlcolorcode
  - qlstephen
  - qlmarkdown
  - quicklook-json
  - quicklook-csv
  - screenhero
  - sequel-pro
  - shimo
  - sketch
  - slack
  - smartgit
  - suspicious-package
  - transmit

Packages (installed with Homebrew):

  - ag
  - dockutil
  - sudolikeaboss

Atom Packages (installed with [hnakamur.atom-packages](https://galaxy.ansible.com/hnakamur/atom-packages/))

  - atom-beautify
  - autocomplete-ansible
  - base16-syntax
  - file-icons
  - highlight-selected
  - language-ansible
  - linter
  - linter-ansible-linting
  - linter-shellcheck
  - minimap
  - minimap-find-and-replace
  - minimap-highlight-selected
  - pretty-json
  - sort-lines
  - Sublime-Style-Column-Selection
  - tablr

Node Versions (installed with nodenv)

  - 7.7.1

Node Packages (installed with NPM):

  - gistup

Python Versions (installed with pyenv)

  - 2.6.9

Python Packages (installed with PIP):

  - ansible-lint


My [dotfiles](https://github.com/kadaan/dotfiles) are also installed into the current user's home directory, including the `.osx` dotfile for configuring many aspects of macOS for better performance and ease of use.

Finally, there are a few other preferences and settings added on for various apps and services.

## Future additions

### Things that still need to be done manually

It's my hope that I can get the rest of these things wrapped up into Ansible playbooks soon, but for now, these steps need to be completed manually (assuming you already have Xcode and Ansible installed, and have run this playbook).

  1.  Install all the apps that aren't yet in this setup (see below).
  2.  Set trackpad tracking rate.
  3.  Set mouse tracking rate.
  4.  Configure extra Mail and/or Calendar accounts (e.g. Google, Exchange, etc.).
  5.  Configure 1Password.
  6.  Configure HyperDock in System Preferences.
  7.  Configure CrashPlan.
  8.  Configure Dropbox.
  9.  Configure Google Drive.
  10. Configure Slack.
  11. Configure Shimo and set storage directory to iCloud/Shimo.
  12. Configure Copy'em Paste.
  13. Configure Intellij Idea [Setting Repository](https://bitbucket.org/jbaranick/intellij-settings.git).
  14. Configure ScreenHero.
  15. Configure Dash with sync to ~/iCloud Drive/Dash.
  16. Configure sudolikeaboss.
  17. Configure default-folder-x.
  18. Add self-signed certs to Intellij Idea and JDK cacerts.
  19. Run `mackup restore`

### Applications/packages to be added:

These are mostly direct download links, some are more difficult to install because of custom installers or other nonstandard install quirks:

  - [1Password Safari Extension](https://agilebits.com/onepassword/extensions)
  - [AdBlockPro Safari Extension](https://update.adblockplus.org/latest/adblockplussafari.safariextz)
  - [Okta Safari Extension](https://ensighten.okta.com/app/UserHome#)
  - Install little-snitch: /usr/local/Caskroom/little-snitch/3.7.2/Little Snitch Installer.app
  - [Jabra Suite for mac](http://www.jabra.com/software-and-services/jabra-suite-for-mac)

### Intellij Idea plugins to be added:

These are the Intellij Idea plugins that need to be manually installed:

  - .ignore
  - ANTLR v4 grammer plugin
  - BashSupport
  - CodeGlance
  - Lombok Plugin

### Reboot

## Testing the Playbook

Many people have asked me if I often wipe my entire workstation and start from scratch just to test changes to the playbook. Nope! Instead, I posted instructions for how I build a [Mac OS X VirtualBox VM](https://github.com/kadaan/mac-osx-virtualbox-vm), on which I can continually run and re-run this playbook to test changes and make sure things work correctly.

Additionally, this project is [continuously tested on Travis CI's macOS infrastructure](https://travis-ci.org/kadaan/mac-dev-playbook).

## Author

[kadaan/mac-dev-playbook](https://github.com/kadaan/mac-dev-playbook), 2017 (originally inspired by
[Jeff Geerling](http://www.jeffgeerling.com/), 2014 (originally inspired by [MWGriffin/ansible-playbooks](https://github.com/MWGriffin/ansible-playbooks))).
